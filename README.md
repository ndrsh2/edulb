/* -------------------------------------------------------------------------
EduLB - The educational lattice Boltzmann code

Copyright (C) 2013 Andreas Hantsch (edulb@gmx-topmail.de)
Version 0.5

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
--------------------------------------------------------------------------*/

0. Dependencies
EduLB itself is operable on operating system with a C++ compiler installed. For the pre- and postprocessing it is necessary to install python and gnuplot. Having inkscape installed is recommended for geometry generation. For enhanced comfort it is suggested to have make installed.

I. Installation
Installation is quite easy. Just unzip the tar.gz to have a test run.

II. Creating a geometry
The geometry file is defined with a PNG file. I usually create a vector graphic with inkscape and export this as PNG in the desired resolution. To create the geometry file (./system/geometry) the script png2geometry.py is provied. The path to the geometry file has to be inserted into the ./system/controlDict file.


III. Defining all the parameters
The program can be totally controlled with the ./system/controlDict file. 
TMAX    ... maximum number of time steps to be computed
DENSIT0 ... initial density, usually 0
UY0     ... initial y-component of the velocity field
KINVISC ... kinematic viscosity in LB units, usually 0 << KINVISC < infinity
RE      ... Reynolds number based on channel height, for determining UX0
DT_WRIT ... time interval for writing results to file
DT_DENS ... time interval for having some statistics on stdout
GEOFILE ... path to geometry file

IV. Compiling and running the program
Compiling and starting program, and doing the postprocessing is performed with make

./png2geometry.py $PATH-TO-PNGFILE
make comp ... compiling
make run  ... running
make post ... postprocessing

make comp run post ... doing all together
